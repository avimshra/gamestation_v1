import Vue from 'vue'
import App from './App.vue'
import router from './router'


import BootstrapVue from 'bootstrap-vue'



import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'






Vue.use(BootstrapVue)

Vue.config.productionTip = false

new Vue({
  
  router,
  render: h => h(App),
  data() {
    return {
        //message show
        chatBoxShow: false,

        //sideNav
      sideNav:false,

      //sidenavname
      
    }
  },
  methods: {
    handleResize() {
      this.windowHeight = window.innerHeight;
      this.windowWidth = window.innerWidth;
      if (this.windowWidth > 991) {
        this.$root.chatBoxShow = true;
      } else {
        this.$root.chatBoxShow = false;
      }
    },
  },
}).$mount('#app')
